from flask import Blueprint
from flask import request
from flask import jsonify
from ..core.book import models as book_models

book_api_blueprint = Blueprint('book', __name__, url_prefix='/api')

@book_api_blueprint.route('/book', methods=['POST'])
def create():
    """Create a new book and Return it.
    """
    book_data = request.get_json()
    book = book_models.create_book(book_data['title'], book_data['author'])
    r = {
        'title': book['title'],
        'author': book['author']
    }
    return jsonify(r)
