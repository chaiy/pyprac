import mongomock


def test_mongomock_connection(testing_app):
    from pyprac.dataaccess import current_data_access
    assert testing_app.config['TESTING'] is True
    assert isinstance(current_data_access.database, mongomock.Database)

