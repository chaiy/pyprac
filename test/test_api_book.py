from flask import json

book_title = 'MyBook'
book_author = 'AlbertE.'


def test_create_book_api(testing_app):
    with testing_app.test_client() as client:
        response = client.post('/api/book', headers={'Content-Type': 'application/json'},
                               data=json.dumps({'title': book_title, 'author': book_author}))
        result = json.loads(response.data)

        assert response.status_code == 200
        assert result['title'] == book_title
        assert result['author'] == book_author

