import pyprac
import pymongo

client = pymongo.MongoClient()
database = client['pyprac']

app = pyprac.create_app(database)

if __name__ == '__main__':
    app.run()


